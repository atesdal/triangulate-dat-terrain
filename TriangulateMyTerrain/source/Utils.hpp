#ifndef UTILS_HEADER_
#define UTILS_HEADER_
#pragma once

#include <tgl/tgl.h>

namespace Util
{
	const static GLuint NullID{ 0 };



	//Vertex shader vars
	enum VertexAttribIndices {
		EVertexPos = 0,
		EVertexNormal = 1,
		EVertexTangent = 2,
		EVertexTexCoord = 3
	};

	//Fragment shader vars
	enum FragmentAttribIndices {
		EFragCol = 0
	};

	enum TextureIndices {
		EDiffuse = 0,
		ESpecular = 1
	};
}

#endif // !UTILS_HEADER_
